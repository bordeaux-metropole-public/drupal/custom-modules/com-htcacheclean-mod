<?php

namespace Drupal\htcacheclean\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HtCacheCleanDialog. The config form for the htcacheclean module.
 *
 * @package Drupal\htcacheclean\Form
 */
class HtCacheCleanDialog extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'htcacheclean_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['htcacheclean.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('htcacheclean.settings');

    $form['htcacheclean_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('htcacheclean binary path'),
      '#default_value' => $config->get('htcacheclean_path'),
    ];
    $form['cache_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cache directory path'),
      '#default_value' => $config->get('cache_path'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('htcacheclean.settings');

    $config
      ->set('htcacheclean_path', $form_state->getValue('htcacheclean_path'))
      ->set('cache_path', $form_state->getValue('cache_path'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
